# [s4j.fr](https://s4j.fr)

Le site pour échanger avec des personnes passées à la semaine de 4 jours

## Lancement en local

Installation
```sh
npm i
```

Lancement en mode dev
```sh
npm run serve
```

- [site local](http://localhost:3000)
- [pattern library local](http://localhost:3000/documentation.html)
