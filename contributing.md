# Comment contribuer

Vous avez des informations à ajouter ou a modifier sur [s4j](https://www.s4j.fr) ?

Deux approches possibles :

- **Vous êtes à l'aise avec l'OpenSource** : Soumettez directement une Merge Request avec les changements que vous voulez apporter ;
- **Vous n'êtes pas à l'aise avec ces outils** : Créez une [nouvelle issue](https://gitlab.com/ideme1/s4j/-/issues/new) en précisant les changements que vous voudriez voir apportés.

## Ajouter une entreprise

Pour ajouter une entreprise il faut modifier le fichier [index.pug](src/index.pug) et ajouter un bloc `company` en respectant l'ordre alhabétique de la page : 

```
+company(
  {
    name: 'Ideme',
    website: 'https://ideme.fr',
    location: 'Lyon',
    details: {
      employees: '10 - 50',
      contract: '174 jours / an',
      industry: 'Conseil',
    },
    contacts: [
      {
        name: 'Pascaline LAROSE',
        role: 'Présidente',
        linkedin: 'https://www.linkedin.com/in/pascaline-larose-a91a5a2a/'
      },
      {
        name: 'Colin DAMON',
        role: 'Dev.',
        linkedin: 'https://www.linkedin.com/in/colin-damon/'
      }
    ]
  }
)
```

> Les informations obligatoires sont `name` et `website`, toutes les autres informations sont facultatives.

Une fois les modifications souhaitées faites il faut [soumettre une Merge Request](https://gitlab.com/ideme1/s4j/-/merge_requests/new)

## Supprimer une entreprise

Pour supprimer une entreprise il faut modifier le fichier [index.pug](src/index.pug)  et supprimer le bloc `company` que vous voulez voir supprimé.  

Une fois les modifications souhaitées faites il faut [soumettre une Merge Request](https://gitlab.com/ideme1/s4j/-/merge_requests/new)
